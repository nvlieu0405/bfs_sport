//
//  ProductEntity.swift
//  BFSSport
//
//  Created by Văn Liệu on 09/04/2022.
//

import Foundation
struct ProductElement: Codable {
    let id, name, image: String
    let content, price: String
    let type: String
    let createdAt: String
    let updatedAt, productID, releaseDate: String
    let yesPercentage: String
    let qualityAvailable: Int
    enum CodingKeys: String, CodingKey {
        case id, name,image
        case content, price
        case type
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case productID = "product_id"
        case releaseDate = "release_date"
        case yesPercentage = "yes_percentage"
        case qualityAvailable
    }
}

enum Colorway: String, Codable {
    case blackDarkCharcoalUniversityGold = "Black/Dark Charcoal-University Gold"
    case blackLightFusionRedWhite = "Black/Light Fusion Red-White"
    case blackLightGraphiteDarkGreyBordeaux = "Black/Light Graphite-Dark Grey-Bordeaux"
    case blackPineGreenCementGreyWhite = "Black/Pine Green-Cement Grey-White"
    case empty = ""
    case jadeHorizonLightSilverAnthracitePinkGlaze = "Jade Horizon/Light Silver-Anthracite-Pink Glaze"
    case pollenBlackWhite = "Pollen/Black-White"
}

enum Resale: String, Codable {
    case medium = "medium"
}

enum StockxMake: String, Codable {
    case empty = ""
    case jordan1Low = "Jordan 1 Low"
    case jordan1RetroHigh = "Jordan 1 Retro High"
    case jordan3Retro = "Jordan 3 Retro"
    case jordan5Retro = "Jordan 5 Retro"
    case jordan6Retro = "Jordan 6 Retro"
    case jordan9Retro = "Jordan 9 Retro"
}

enum StockxModel: String, Codable {
    case bordeaux = "Bordeaux"
    case childrenSArtGS = "Children's Art (GS)"
    case darkCharcoalUniversityGold = "Dark Charcoal University Gold"
    case empty = ""
    case jadeHorizon = "Jade Horizon"
    case pineGreen = "Pine Green"
    case pollen = "Pollen"
}

enum StockxName: String, Codable {
    case empty = ""
    case jordan1LowChildrenSArtGS = "Jordan 1 Low Children's Art (GS)"
    case jordan1RetroHighPollen = "Jordan 1 Retro High Pollen"
    case jordan3RetroPineGreen = "Jordan 3 Retro Pine Green"
    case jordan5RetroJadeHorizon = "Jordan 5 Retro Jade Horizon"
    case jordan6RetroBordeaux = "Jordan 6 Retro Bordeaux"
    case jordan9RetroDarkCharcoalUniversityGold = "Jordan 9 Retro Dark Charcoal University Gold"
}

enum StockxTickerSymbol: String, Codable {
    case empty = ""
    case jbJo1Lcabl = "JB-JO1LCABL"
    case jbJo1Rhpbkgd = "JB-JO1RHPBKGD"
    case jbJo3Rpgbp = "JB-JO3RPGBP"
    case jbJo5Rjhjl = "JB-JO5RJHJL"
    case jbJo6Rbbl = "JB-JO6RBBL"
    case jbJo9Rdcubd = "JB-JO9RDCUBD"
}

enum StockxURL: String, Codable {
    case airJordan1LowChildrensArtGs = "air-jordan-1-low-childrens-art-gs"
    case airJordan1RetroHighPollen = "air-jordan-1-retro-high-pollen"
    case airJordan3RetroPineGreen = "air-jordan-3-retro-pine-green"
    case airJordan5RetroJadeHorizon = "air-jordan-5-retro-jade-horizon"
    case airJordan6RetroBordeaux = "air-jordan-6-retro-bordeaux"
    case airJordan9RetroDarkCharcoalUniversityGold = "air-jordan-9-retro-dark-charcoal-university-gold"
    case empty = ""
}

enum TypeEnum: String, Codable {
    case sneakers = "sneakers"
    case jordan = "jordan"
    case adidas = "adidas"
    case nike = "nike"
    case reebok = "reebok"
}

typealias Product = [ProductElement]
