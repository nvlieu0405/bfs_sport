//
//  OrderEntity.swift
//  BFSSport
//
//  Created by Văn Liệu on 28/04/2022.
//

import Foundation
struct OrderEntity{
    var orderId: Int
    var accountId: String
    var addressCustomer: String
    var nameCustomer: String
    var numberphone: String
    var orderStatus: Int
    var totalMoney: Float
    var createAt: String
    var list: [CartEntity]
}
struct CartEntity{
    var id: Int
    var number: Int
    var price: Int
    var image: String
    var name: String
}
