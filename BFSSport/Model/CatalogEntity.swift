//
//  CatalogEntity.swift
//  BFSSport
//
//  Created by Văn Liệu on 02/05/2022.
//

import Foundation
struct CatalogEntity{
    var id: Int
    var title: String
}
