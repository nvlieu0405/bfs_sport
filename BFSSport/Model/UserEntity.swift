//
//  UserEntity.swift
//  BFSSport
//
//  Created by Văn Liệu on 29/04/2022.
//

import Foundation
struct MenuTable{
    var title: String
    var image: String
}
struct User{
    var id: String
    var firstname: String
    var lastname: String
    var numberphone: String
    var address: String
}
