//
//  SearchViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 13/04/2022.
//

import UIKit
import Alamofire
import FirebaseDatabase
class SearchViewController: UIViewController {
    
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var ref: DatabaseReference!
    var list: [ProductElement] = []
    var listSearch: [ProductElement] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        setupTable()
        getData()
        title = "Search"
        searchBar.placeholder = "Search"
       
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        indicator.isHidden = true
    }
    func setupTable(){
        searchTableView.delegate = self
        searchTableView.dataSource = self
        searchTableView.register(UINib(nibName: "KindViewCell", bundle: nil), forCellReuseIdentifier: "KindViewCell")
        searchBar.delegate = self
    }
    private func getData(){
        ref.child("sanpham").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return;
            }
            let userName = snapshot.value as? NSDictionary
            guard let userName = userName else{
                return
            }
            for item in userName{
                let product = item.value as? NSDictionary
                let idp: String = product?.value(forKey: "productID") as? String ?? ""
                let name: String = product?.value(forKey: "name") as? String ?? ""
                let image: String = product?.value(forKey: "image") as? String  ?? ""
                let content: String = product?.value(forKey: "content") as? String ?? ""
                let price: String = product?.value(forKey: "price") as? String ?? ""
                let createAt: String = product?.value(forKey: "createAt") as? String ?? ""
                let updateAt: String = product?.value(forKey: "updateAt") as? String ?? ""
                let releaseDate: String = product?.value(forKey: "releaseDate") as? String ?? ""
                let rate: String = product?.value(forKey: "rate") as? String ?? ""
                let type: String = product?.value(forKey: "type") as? String ?? ""
                let qualityAvailable: Int = product?.value(forKey: "quantityAvailable") as? Int ?? 0
                let newPro = ProductElement(id: idp, name: name, image: image, content: content, price: price, type: type, createdAt: createAt, updatedAt: updateAt, productID: idp, releaseDate: releaseDate, yesPercentage: rate, qualityAvailable: qualityAvailable)
                self.addToList(newPro)
            }
            self.searchTableView.reloadData()
        })
    }
    func addToList(_ product: ProductElement){
        list.append(product)
    }
}
extension SearchViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listSearch.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "KindViewCell", for: indexPath) as? KindViewCell else {
            return UITableViewCell()
        }
        cell.bindData(listSearch[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        200
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailViewController()
        vc.product = listSearch[indexPath.row]
        vc.list = list
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension SearchViewController: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        listSearch.removeAll()
        if searchBar.text != "" {
            self.indicator.isHidden = true
            for item in list{
                if item.name.lowercased().contains(searchBar.text?.lowercased() ?? ""){
                    listSearch.append(item)
                }
            }
            self.searchTableView.reloadData()
            
            self.searchTableView.isHidden = false
            
        }else{
            self.searchTableView.isHidden = true
            self.indicator.isHidden = false
        }
    }
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.placeholder = "Enter product name"
        searchBar.showsCancelButton = true
        indicator.startAnimating()
        indicator.isHidden = false
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchTableView.isHidden = true
        indicator.isHidden = true
        searchBar.resignFirstResponder()
    }
}
