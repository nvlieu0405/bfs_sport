//
//  HeaderReusableView.swift
//  BFSSport
//
//  Created by Văn Liệu on 12/04/2022.
//

import UIKit

class HeaderReusableView: UICollectionReusableView {

    @IBOutlet private weak var label: UILabel!
    
    @IBOutlet weak var btnSeeAll: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnSeeAll.layer.cornerRadius = btnSeeAll.bounds.height / 2
        btnSeeAll.layer.borderWidth = 1
        
    }
    func setupData(text: String){
        label.text = text
    }
    
}
