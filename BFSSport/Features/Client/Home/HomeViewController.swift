//
//  HomeViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 10/04/2022.
//

import UIKit
import Alamofire
import FirebaseDatabase
class HomeViewController: UIViewController {
    
    @IBOutlet weak var imageSlideHeightContraist: NSLayoutConstraint!
    @IBOutlet weak var slideShowImage: UIImageView!
    @IBOutlet weak var productCollection: UICollectionView!
    
    var list: [ProductElement]?
    
    var listAdidas: [ProductElement] = [ ]
    var listNike: [ProductElement] = [ ]
    var listJordan: [ProductElement] = [ ]
    var listReebok: [ProductElement] = [ ]
    
    var listP: [[ProductElement]] = []
    
    var listCata: [CatalogEntity] = []
    var ref: DatabaseReference!
    var imageNames = ["jordan","nike","adidas","default", "reebok"]
    override func viewDidLoad() {
        super.viewDidLoad()
        //fetchListProduct()
        
        ref = Database.database().reference()

        productCollection.delegate = self
        productCollection.dataSource = self
        productCollection.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCell")
        productCollection.register(UINib(nibName: "HeaderReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:"HeaderReusableView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
       // self.imageSlideHeightContraist.constant = 150
        getCataData()
        getData()
        
    }
    
    @objc func clickSeeAll(sender: UIButton){
        let list = listP[sender.tag]
        let vc = ListViewController()
        vc.list = list
        vc.title = listCata[sender.tag].title
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomeViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        listCata.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if listP[section].count > 4 {
            return 4
        } else {
            return listP[section].count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell else{
            return UICollectionViewCell()
            
        }
        let list = listP[indexPath.section]
        cell.bindData(list[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderReusableView", for: indexPath) as? HeaderReusableView{
            sectionHeader.setupData(text: listCata[indexPath.section].title)
            sectionHeader.btnSeeAll.tag = indexPath.section
            sectionHeader.btnSeeAll.addTarget(self, action: #selector(clickSeeAll(sender:)), for: .touchUpInside)
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: productCollection.frame.width, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DetailViewController()
        let list = listP[indexPath.section]
        vc.list = list
        vc.product = list[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}
extension HomeViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: productCollection.frame.width / 2.2, height: productCollection.frame.height / 2.2)
    }
}
extension HomeViewController {
    private func getData(){
        ref.child("sanpham").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return;
            }
            let userName = snapshot.value as? NSDictionary
            guard let userName = userName else{
                return
            }
            for item in userName{
                let product = item.value as? NSDictionary
                let idp: String = product?.value(forKey: "productID") as? String ?? ""
                let name: String = product?.value(forKey: "name") as? String ?? ""
                let image: String = product?.value(forKey: "image") as? String  ?? ""
                let content: String = product?.value(forKey: "content") as? String ?? ""
                let price: String = product?.value(forKey: "price") as? String ?? ""
                let createAt: String = product?.value(forKey: "createAt") as? String ?? ""
                let updateAt: String = product?.value(forKey: "updateAt") as? String ?? ""
                let releaseDate: String = product?.value(forKey: "releaseDate") as? String ?? ""
                let rate: String = product?.value(forKey: "rate") as? String ?? ""
                let type: String = product?.value(forKey: "type") as? String ?? ""
                let qualityAvailable: Int = product?.value(forKey: "quantityAvailable") as? Int ?? 0
                let newPro = ProductElement(id: idp, name: name, image: image, content: content, price: price, type: type, createdAt: createAt, updatedAt: updateAt, productID: idp, releaseDate: releaseDate, yesPercentage: rate,qualityAvailable: qualityAvailable)
                self.addToList(newPro)
            }
            self.productCollection.reloadData()
        })
    }
    private func addToList(_ product: ProductElement){
        
        for index in 0...listCata.count - 1 {
            if listCata[index].id == Int (product.type) {
                listP[index].append(product)
            }
        }
    }
    private func addProductFirebase(product: ProductElement){
        self.ref.child("sanpham").child(product.id).child("name").setValue(product.name)
        self.ref.child("sanpham").child(product.id).child("productID").setValue(product.productID)
        self.ref.child("sanpham").child(product.id).child("image").setValue(product.image)
        self.ref.child("sanpham").child(product.id).child("content").setValue(product.content)
        self.ref.child("sanpham").child(product.id).child("price").setValue(product.price)
        self.ref.child("sanpham").child(product.id).child("createAt").setValue(product.createdAt)
        self.ref.child("sanpham").child(product.id).child("updateAt").setValue(product.updatedAt)
        self.ref.child("sanpham").child(product.id).child("releaseDate").setValue(product.releaseDate)
        self.ref.child("sanpham").child(product.id).child("rate").setValue(product.yesPercentage)
        self.ref.child("sanpham").child(product.id).child("quantityAvailable").setValue(100)
        var type: String = ""
        if product.name.lowercased().contains(TypeEnum.adidas.rawValue){
            type = "adidas"
        }
        if product.name.lowercased().contains(TypeEnum.nike.rawValue){
            type = "nike"
        }
        if product.name.lowercased().contains(TypeEnum.jordan.rawValue){
            type = "jordan"
        }
        if product.name.lowercased().contains(TypeEnum.reebok.rawValue){
            type = "reebok"
        }
        self.ref.child("sanpham").child(product.id).child("type").setValue(type)
    }
    func add(list: [ProductElement]?){
        guard let list = list else {
            return
        }
        for i in 0...list.count-1{
            addProductFirebase(product: list[i])
        }
    }
    private func fetchListProduct() {
        let pathURL: String = "http://soleinsider.com/mobileapi/releaseDatesUnformatted/"
        AF.request(pathURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil).response { [weak self] (responseData) in
            guard let strongSelf = self, let data = responseData.data else {
                return
            }
            do {
                let character = try JSONDecoder().decode([ProductElement].self, from: data)
                if character != nil {
                    DispatchQueue.main.async {
                        self?.list = character
                        self?.add(list: self?.list)
                    }
                }
            } catch {
                print("Error \(error)")
            }
            
        }
    }
    
}
extension HomeViewController {
    private func getCataData(){
        listCata.removeAll()
        ref.child("DanhMuc").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            let cata = snapshot.value as? [NSDictionary?]
            
            if cata == nil {
                let cata2 = snapshot.value as? NSDictionary
                guard let cata2 = cata2 else {
                    return
                }
                for item2 in cata2 {
                    let item = item2.value as? NSDictionary
                    if item != nil {
                        let title: String = item?.value(forKey: "title") as? String ?? ""
                        let id: Int = item?.value(forKey: "id") as? Int ?? 0
                        let newCat = CatalogEntity(id: id,title: title)
                        self.addToList(newCat)
                    }
                }
            }else{
                guard let cata = cata else {
                    return
                }
                for item in cata {
                    if item != nil {
                        let title: String = item?.value(forKey: "title") as? String ?? ""
                        let id: Int = item?.value(forKey: "id") as? Int ?? 0
                        let newCat = CatalogEntity(id: id,title: title)
                        self.addToList(newCat)
                    }
                }
            }
            self.listP = [[ProductElement]](repeating: [], count: self.listCata.count)
            self.productCollection.reloadData()
        })
    }
    func addToList(_ newCat: CatalogEntity){
        listCata.append(newCat)
    }

}


