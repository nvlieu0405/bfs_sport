//
//  DetailViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 10/04/2022.
//

import UIKit
import Kingfisher
class DetailViewController: UIViewController {
    
    @IBOutlet weak var stepperNumber: UIStepper!
    @IBOutlet weak var numberBuy: UILabel!
    @IBOutlet weak var buttonAddCart: UIButton!
    @IBOutlet weak var similarProduct: UITableView!
    @IBOutlet weak var contentProduct: UILabel!
    @IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var priceProduct: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    var product: ProductElement?
    var list: [ProductElement] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadView()
        setupTable()
    }
    func setupTable(){
        similarProduct.delegate = self
        similarProduct.dataSource = self
        similarProduct.register(UINib(nibName: "KindViewCell", bundle: nil), forCellReuseIdentifier: "KindViewCell")
    }
    func reloadView(){
        guard let product = product else {
            return
        }
        numberBuy.text = "0"
        stepperNumber.value = 0
        contentProduct.text = "\(product.content.html2String.htmlToString)"
        let urlImg: String = "http://soleinsider.com/public/products/\(product.image)"
        let url = URL(string: urlImg)
        let processor = DownsamplingImageProcessor(size: imageProduct.bounds.size)
                     |> RoundCornerImageProcessor(cornerRadius: 0)
        imageProduct.kf.indicatorType = .activity
        imageProduct.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholder"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        stepperNumber.addTarget(self, action: #selector(stepValueChanged(_ :)), for: .valueChanged)
    }
   @objc func stepValueChanged(_ sender: UIStepper!){
       numberBuy.text = "\(Int(stepperNumber.value))"
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        title = "Detail"
    }

    @IBAction func clickAddCart(_ sender: Any) {
        if stepperNumber.value > 0{
            guard let product = product else {
                return
            }
            if DatabaseManager.shared.checkCart(id: Int(product.id) ?? 0){
                DatabaseManager.shared.updateCart(with: Int(product.id) ?? 0, number: Int(stepperNumber.value))
            }else{
                DatabaseManager.shared.addCart(id: Int(product.id) ?? 0, number: Int(stepperNumber.value),name: product.name, price: Float(product.price) ?? 0,image: product.image ?? "")
            }
            let alert = UIAlertController(title: "Success!", message: "Add to cart complete!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default)
            alert.addAction(okAction)
            present(alert, animated: true)
        }else{
            let alert = UIAlertController(title: "Error!", message: "Please choose to buy quantity!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .destructive)
            alert.addAction(okAction)
            present(alert, animated: true)
        }
        
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}
extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}
extension String{
    var htmlToString: String{
        guard let data = data(using: .utf8) else {
            return ""
        }
        do{
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil).string
        }catch let error as NSError {
            print(error.localizedDescription)
            return ""
        }
    }
}
extension DetailViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Similar Product"
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if list.count > 5{
            return 5
        }else{
            return list.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "KindViewCell", for: indexPath) as? KindViewCell else {
            return UITableViewCell()
        }
        cell.bindData(list[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        150
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        product = list[indexPath.row]
        reloadView()
    }
}
