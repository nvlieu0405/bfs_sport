//
//  AcceptOrderViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 13/04/2022.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
class AcceptOrderViewController: UIViewController {
    @IBOutlet weak var nameCustomer: UITextField!
    @IBOutlet weak var addressCustomer: UITextField!
    @IBOutlet weak var numberPhoneCustomer: UITextField!
    var number: Int = 0
    var ref: DatabaseReference!
    var list: [Cart] = []
    var status: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        list = DatabaseManager.shared.getListCart()
        getListOrder()
        nameCustomer.delegate = self
        addressCustomer.delegate = self
        numberPhoneCustomer.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        title = "Information line"
    }
    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    func validate(value: String) -> Bool {
                let PHONE_REGEX = "^\\d{10}$"
                let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
                let result = phoneTest.evaluate(with: value)
                return result
            }
    @IBAction func clickOrder(_ sender: Any) {
        if nameCustomer.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || addressCustomer.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || numberPhoneCustomer.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || !validate(value: numberPhoneCustomer.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""){
            let alert = UIAlertController(title: "Error!", message: "Error validate!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .destructive)
            alert.addAction(okAction)
            present(alert, animated: true)
        }else{
            let currentDate = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let nameCustomer = nameCustomer.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let addressCustomer = addressCustomer.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let numberPhoneCustomer = numberPhoneCustomer.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            let user = Auth.auth().currentUser?.uid
            for item in list{
                self.ref.child("DonHang").child(user ?? "").child("\(number)").child("list").child("\(item.id)").child("id").setValue(item.id)
                self.ref.child("DonHang").child(user ?? "").child("\(number)").child("list").child("\(item.id)").child("image").setValue(item.image)
                self.ref.child("DonHang").child(user ?? "").child("\(number)").child("list").child("\(item.id)").child("name").setValue(item.name)
                self.ref.child("DonHang").child(user ?? "").child("\(number)").child("list").child("\(item.id)").child("number").setValue(item.number)
                self.ref.child("DonHang").child(user ?? "").child("\(number)").child("list").child("\(item.id)").child("price").setValue(item.price)
            }
            self.ref.child("DonHang").child(user ?? "").child("\(number)").child("orderId").setValue(number)
            self.ref.child("DonHang").child(user ?? "").child("\(number)").child("id").setValue(user)
            self.ref.child("DonHang").child(user ?? "").child("\(number)").child("nameCustomer").setValue(nameCustomer)
            self.ref.child("DonHang").child(user ?? "").child("\(number)").child("addressCustomer").setValue(addressCustomer)
            self.ref.child("DonHang").child(user ?? "").child("\(number)").child("numberphone").setValue(numberPhoneCustomer)
            self.ref.child("DonHang").child(user ?? "").child("\(number)").child("status").setValue(status)
            self.ref.child("DonHang").child(user ?? "").child("\(number)").child("totalMoney").setValue(CartViewController.sumOfPrice)
            self.ref.child("DonHang").child(user ?? "").child("\(number)").child("createAt").setValue("\(dateFormatter.string(from: currentDate))")
            DatabaseManager.shared.deleteCart()
            number = number + 1
            let alert = UIAlertController(title: "Success!", message: "Complete!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default){_ in
                let vc = OrderViewController()
                self.navigationController?.pushViewController(vc, animated: true)
                self.tabBarController?.selectedIndex = 3
            }
            alert.addAction(okAction)
            present(alert, animated: true)
        }
    }
    func getListOrder(){
        let user = Auth.auth().currentUser?.uid
        ref.child("DonHang").child(user!).observeSingleEvent(of: .value, with: { snapshot in
          // Get user value
            let order = snapshot.value as? NSDictionary
            if order == nil {
                let order1 =  snapshot.value as? [NSDictionary?]
                guard let order1 = order1 else {
                    return
                }
                for item in order1 {
                    if item != nil {
                        let order2 = item as? NSDictionary
                        let num: Int = order2?.value(forKey: "orderId") as? Int ?? 0
                        self.number = num + 1
                    }
                }
            }else {
                guard let order = order else {
                    return
                }
                for item in order{
                    let order1 = item.value as? NSDictionary
                    let num: Int = order1?.value(forKey: "orderId") as? Int ?? 0
                    self.number = num + 1
                }
            }
            
        }) { error in
          print(error.localizedDescription)
        }
    }
}
extension AcceptOrderViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
}
