//
//  OrderViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 10/04/2022.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
class OrderViewController: UIViewController {
    @IBOutlet weak var textLabel: UILabel!
    var ref: DatabaseReference!
    @IBOutlet weak var orderTable: UITableView!
    var listOrder : [OrderEntity] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        
    }
    func setupTable(){
        orderTable.register(UINib(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
        orderTable.delegate = self
        orderTable.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        orderTable.reloadData()
        let user = Auth.auth().currentUser?.uid
        if user != nil{
            orderTable.isHidden = false
            textLabel.isHidden = true
            setupTable()
        }else{
            orderTable.isHidden = true
            textLabel.isHidden = false
        }
        getListOrder()
    }
    func getListOrder(){
        let user = Auth.auth().currentUser?.uid
        guard let user = user else{
            return
        }
        ref.child("DonHang").child(user).getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            let order = snapshot.value as? [NSDictionary?]
            if order == nil {
                let order1 = snapshot.value as? NSDictionary
                guard let order1 = order1 else {
                    return
                }
            } else {
                guard let order = order else{
                    return
                }
                self.listOrder.removeAll()
                for item in order {
                    if item != nil {
                        let id: Int = item?.value(forKey: "orderId") as? Int ?? 0
                        let accountID: String = item?.value(forKey: "id") as? String ?? ""
                        let addressCustomer = item?.value(forKey: "addressCustomer") as? String ?? ""
                        let nameCustomer = item?.value(forKey: "nameCustomer") as? String ?? ""
                        let numberphone = item?.value(forKey: "numberphone") as? String ?? ""
                        let createAt = item?.value(forKey: "createAt") as? String ?? ""
                        let status = item?.value(forKey: "status") as? Int ?? 0
                        let totalMoney = item?.value(forKey: "totalMoney") as? Float ?? 0.0
                        var listP: [CartEntity] = []
                        let list = item?.value(forKey: "list") as? NSDictionary
                        guard let list = list else{
                            return
                        }
                        for product2 in list{
                            let product = product2.value as? NSDictionary
                            let id: Int = product?.value(forKey:"id") as? Int ?? 0
                            let image: String = product?.value(forKey: "image") as? String ?? ""
                            let name: String = product?.value(forKey: "name") as? String ?? ""
                            let number: Int = product?.value(forKey: "number") as? Int ?? 0
                            let price: Int = product?.value(forKey: "price") as? Int ?? 0
                            let cart = CartEntity(id: id, number: number, price: price, image: image, name: name)
                            listP.append(cart)
                        }
                        let orderEntity = OrderEntity(orderId: id, accountId: accountID, addressCustomer: addressCustomer, nameCustomer: nameCustomer, numberphone: numberphone, orderStatus: status,totalMoney: Float(totalMoney) ,createAt: createAt, list: listP)
                        self.addToList(orderEntity)
                    }
                }
            }
            self.orderTable.reloadData()
        })
    }
    func addToList(_ orderEntity: OrderEntity){
        listOrder.append(orderEntity)
    }
}
extension OrderViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listOrder.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as? OrderCell else{
            return UITableViewCell()
        }
        cell.bindData(listOrder[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
        
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .normal, title: "Cancel") { (action, view, completionHandler) in
            let alert = UIAlertController(title: "Warning!", message: "Are you sure cancel??", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default){_ in
                
                self.ref.child("DonHang").child("\(self.listOrder[indexPath.row].accountId)").child("\(self.listOrder[indexPath.row].orderId)").child("status").setValue(3)
                
                if self.listOrder[indexPath.row].orderStatus == 2 {
                    for item in self.listOrder[indexPath.row].list{
                        let number = self.getNumberById(item.id)
                        let update = number + item.number
                        let childValue = ["sanpham/\(item.id)/quantityAvailable": update]
                        self.ref.updateChildValues(childValue)
                    }
                }
                self.getListOrder()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true)
        }
        delete.image = UIImage(systemName: "trash")
        delete.backgroundColor = .red
        let swipe = UISwipeActionsConfiguration(actions: [delete])
        return swipe
    }
    func getNumberById(_ id: Int) -> Int{
        var number: Int = 100
        ref.child("sanpham/\(id)/quantityAvailable").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            number = snapshot.value as? Int ?? 0
            
        })
        return number
    }
}
