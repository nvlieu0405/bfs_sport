//
//  OrderCell.swift
//  BFSSport
//
//  Created by Văn Liệu on 28/04/2022.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var numberphone: UILabel!
    @IBOutlet weak var totalMoney: UILabel!
    @IBOutlet weak var createAt: UILabel!
    @IBOutlet weak var addressCustomer: UILabel!
    @IBOutlet weak var nameCustomer: UILabel!
    @IBOutlet weak var imageCollection: UICollectionView!
    var listCart: [CartEntity] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCollection()
    }
    func setupCollection(){
        imageCollection.dataSource = self
        imageCollection.delegate = self
        imageCollection.register(UINib(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: "ImageCell")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func bindData(_ order: OrderEntity){
        listCart = order.list
        nameCustomer.text = "Customer's name: \(order.nameCustomer)"
        addressCustomer.text = "Address: \(order.addressCustomer)"
        createAt.text = "Booking date: \(order.createAt)"
        numberphone.text = "Numberphone: \(order.numberphone)"
        totalMoney.text = "Total money: \(order.totalMoney) $"
        switch order.orderStatus{
        case 0:
            status.text = "Wait for confirmation"
            status.textColor = UIColor.systemBlue
        case 1:
            status.text = "Delivering"
            status.textColor = UIColor.systemYellow
        case 2:
            status.text = "Delivered"
            status.textColor = UIColor.systemGreen
        case 3:
            status.text = "Cancelled"
            status.textColor = UIColor.red
        default:
            status.text = "Wait for confirmation"
        }
        imageCollection.reloadData()
    }
    
}
extension OrderCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        listCart.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCell else{
            return UICollectionViewCell()
        }
        cell.bindData(listCart[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: imageCollection.bounds.width / 3, height: imageCollection.bounds.height)
    }
}
