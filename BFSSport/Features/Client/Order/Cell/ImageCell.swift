//
//  ImageCell.swift
//  BFSSport
//
//  Created by Văn Liệu on 13/05/2022.
//

import UIKit
import Kingfisher
class ImageCell: UICollectionViewCell {

    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func bindData(_ data: CartEntity){
       numberLabel.text = " x \(data.number)"
        let urlImg: String = "http://soleinsider.com/public/products/\(data.image)"
        let url = URL(string: urlImg)
        let processor = DownsamplingImageProcessor(size: imageProduct.bounds.size)
                     |> RoundCornerImageProcessor(cornerRadius: 10)
        imageProduct.kf.indicatorType = .activity
        imageProduct.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholder"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
    }
}

