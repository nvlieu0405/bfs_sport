//
//  TabbarController.swift
//  BFSSport
//
//  Created by Văn Liệu on 10/04/2022.
//

import UIKit

class TabbarController: UITabBarController {
    
    private var homeViewController: HomeViewController?
    private var orderViewController: OrderViewController?
    private var newsViewController: SearchViewController?
    private var userViewController: UserViewController?
    private var cartViewController: CartViewController?
    private var subviewControllers:[UIViewController] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        homeViewController = HomeViewController()
        orderViewController = OrderViewController()
        newsViewController = SearchViewController()
        cartViewController = CartViewController()
        userViewController = UserViewController()
        
        guard let homeViewController = homeViewController else {
            return
        }
        guard let orderViewController = orderViewController else {
            return
        }

        guard let cartViewController = cartViewController else {
            return
        }

        guard let newsViewController = newsViewController else {
            return
        }
        guard let userViewController = userViewController else {
            return
        }
        let navigation1 = UINavigationController.init(rootViewController: homeViewController)
        let navigation2 = UINavigationController.init(rootViewController: newsViewController)
        let navigation3 = UINavigationController.init(rootViewController: cartViewController)
        let navigation4 = UINavigationController.init(rootViewController: orderViewController)
        let navigation5 = UINavigationController.init(rootViewController: userViewController)
        subviewControllers.append(navigation1)
        subviewControllers.append(navigation2)
        subviewControllers.append(navigation3)
        subviewControllers.append(navigation4)
        subviewControllers.append(navigation5)
        homeViewController.tabBarItem = UITabBarItem(title: "Home", image: UIImage.init(systemName: "homekit"), selectedImage: UIImage.init(systemName: "homekit"))
        homeViewController.tabBarItem.tag = 0
        newsViewController.tabBarItem = UITabBarItem(title: "Search", image: UIImage.init(systemName: "magnifyingglass"), selectedImage: UIImage.init(systemName: "magnifyingglass"))
        newsViewController.tabBarItem.tag = 1
        cartViewController.tabBarItem = UITabBarItem(title: "Cart", image: UIImage.init(systemName: "cart.fill"), selectedImage: UIImage.init(systemName: "cart.fill"))
        cartViewController.tabBarItem.tag = 2
        orderViewController.tabBarItem = UITabBarItem(title: "Order", image: UIImage.init(systemName: "suitcase.cart.fill"), selectedImage: UIImage.init(systemName: "suitcase.cart.fill"))
        orderViewController.tabBarItem.tag = 3
        userViewController.tabBarItem = UITabBarItem(title: "User", image: UIImage.init(systemName: "person.crop.circle"), selectedImage: UIImage.init(systemName: "person.crop.circle"))
        userViewController.tabBarItem.tag = 4
        UITabBar.appearance().tintColor = UIColor.systemGreen
        self.setViewControllers(subviewControllers, animated: true)
        self.selectedIndex = 0
        self.selectedViewController = navigation1
    }
    
}
