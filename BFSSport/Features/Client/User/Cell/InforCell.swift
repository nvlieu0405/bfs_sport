//
//  InforCell.swift
//  BFSSport
//
//  Created by Văn Liệu on 28/04/2022.
//

import UIKit
import FirebaseAuth
import Firebase
import Kingfisher
class InforCell: UITableViewCell {

    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageAvatar.clipsToBounds = true
        imageAvatar.layer.cornerRadius = imageAvatar.bounds.height/2
        imageAvatar.layer.borderColor = UIColor.systemBlue.cgColor
        imageAvatar.layer.borderWidth = 2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func bindData(_ name: String,_ image: URL?){
        nameUser.text = name
        
        if let image = image {
            let processor = DownsamplingImageProcessor(size: imageAvatar.bounds.size)
                         |> RoundCornerImageProcessor(cornerRadius: 10)
            imageAvatar.kf.indicatorType = .activity
            imageAvatar.kf.setImage(
                with: image,
                placeholder: UIImage(named: "placeholderImage"),
                options: [
                    .processor(processor),
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
        }
       
    }
   
}
