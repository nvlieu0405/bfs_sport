//
//  ItemMenuCell.swift
//  BFSSport
//
//  Created by Văn Liệu on 29/04/2022.
//

import UIKit

class ItemMenuCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func bindData(_ data: MenuTable){
        title.text = data.title
        imageIcon.image = UIImage(named: "\(data.image)")
    }
    
}
