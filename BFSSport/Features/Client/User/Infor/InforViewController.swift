//
//  InforViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 29/04/2022.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseAuth
class InforViewController: UIViewController {

    @IBOutlet weak var numberphone: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var lName: UITextField!
    @IBOutlet weak var fName: UITextField!
    var user: User!
    override func viewDidLoad() {
        super.viewDidLoad()
        numberphone.delegate = self
        address.delegate = self
        lName.delegate = self
        fName.delegate = self
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if let user = user{
            fName.text = user.firstname
            lName.text = user.lastname
            address.text = user.address
            numberphone.text = user.numberphone
        }
        navigationController?.navigationBar.isHidden = false
        title = "User information"
    }
    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    @IBAction func clickUpdate(_ sender: Any) {
        let firstname: String = fName.text ?? ""
        let lastname:String = lName.text ?? ""
        let address:String = address.text ?? ""
        let number:String = numberphone.text ?? ""
        let user = Auth.auth().currentUser?.uid ?? ""
        if firstname != "" , lastname != "", address != "", validate(value: number){
            let db = Firestore.firestore()
            db.collection("user").document("\(user)").setData(["firstname":firstname,"lastname":lastname,"uid":user,"address" : address, "numberphone" : number])
            let alert = UIAlertController(title: "Success!", message: "", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default)
            alert.addAction(okAction)
            present(alert, animated: true)
        }else{
            let alert = UIAlertController(title: "Error!", message: "Error validate!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .destructive)
            alert.addAction(okAction)
            present(alert, animated: true)
        }
        
    }
    func validate(value: String) -> Bool {
                let PHONE_REGEX = "^\\d{10}$"
                let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
                let result = phoneTest.evaluate(with: value)
                return result
            }
}
extension InforViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
}

