//
//  UserViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 10/04/2022.
//

import UIKit
import FirebaseAuth
import Firebase
class UserViewController: UIViewController {
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var btnLogIn: UIButton!
    @IBOutlet weak var inforTable: UITableView!
    var name: String = ""
    var menu: [MenuTable] = []
    var imageUser: URL?
    var userInfor: User!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        setData()
    }
    func setData(){
        menu = [MenuTable(title: "Personal information", image: "user"),
                MenuTable(title: "Change Password", image: "setting"),
                MenuTable(title: "Log out", image: "logout")]
    }
    func setupTable(){
        inforTable.dataSource = self
        inforTable.delegate = self
        inforTable.register(UINib(nibName: "InforCell", bundle: nil), forCellReuseIdentifier: "InforCell")
        inforTable.register(UINib(nibName: "ItemMenuCell", bundle: nil), forCellReuseIdentifier: "ItemMenuCell")
    }
    func getUser(_ user: User){
        userInfor = user
    }
    func get(){
        let user = Auth.auth().currentUser
        if let user = user {
            imageUser = user.photoURL
        }
    }
    
    
    func createProfileChangeRequest(photoUrl: URL? = nil, name: String? = nil, _ callback: ((Error?) -> ())? = nil){
        if let request = Auth.auth().currentUser?.createProfileChangeRequest(){
            if let name = name{
                request.displayName = name
            }
            if let url = photoUrl{
                request.photoURL = url
            }
            request.commitChanges(completion: { (error) in
                callback?(error)
            })
        }
    }
    func updateProfileInfo(withImage image: Data? = nil, name: String? = nil, _ callback: ((Error?) -> ())? = nil){
        guard let user = Auth.auth().currentUser else {
            callback?(nil)
            return
        }
        if let image = image{
            indicator.isHidden = false
            indicator.startAnimating()
            let profileImgReference = Storage.storage().reference().child("profile_pictures").child("\(user.uid).png")

            _ = profileImgReference.putData(image, metadata: nil) { (metadata, error) in
                if let error = error {
                    callback?(error)
                } else {
                    profileImgReference.downloadURL(completion: { (url, error) in
                        if let url = url{
                            self.createProfileChangeRequest(photoUrl: url, name: name, { (error) in
                                callback?(error)
                                self.imageUser = url
                                self.indicator.isHidden = true
                                self.inforTable.reloadData()
                            })
                        }else{
                            callback?(error)
                        }
                    })
                }
            }
        }else if let name = name{
            self.createProfileChangeRequest(name: name, { (error) in
                callback?(error)
            })
        }else{
            callback?(nil)
        }
    }
    
    func getUser(){
        let user = Auth.auth().currentUser?.uid
        let db = Firestore.firestore()
        db.collection("user").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let doc = document.data() as? NSDictionary
                    let uid: String = doc?.value(forKey: "uid") as? String ?? ""
                    if user == uid{
                        let firstname: String = doc?.value(forKey: "firstname") as? String ?? ""
                        let lastname: String = doc?.value(forKey: "lastname") as? String ?? ""
                        let numberphone: String = doc?.value(forKey: "numberphone") as? String ?? ""
                        let address: String = doc?.value(forKey: "address") as? String ?? ""
                        self.name = "\(firstname) \(lastname)"
                        let userNow = User(id: uid, firstname: firstname, lastname: lastname, numberphone: numberphone, address: address)
                        self.getUser(userNow)
                    }
                }
                self.inforTable.reloadData()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        indicator.isHidden = true
        get()
        getUser()
        let user = Auth.auth().currentUser?.uid
        if user == nil {
            inforTable.isHidden = true
            btnLogIn.isHidden = false
        }else{
            inforTable.isHidden = false
            btnLogIn.isHidden = true
        }
        
    }
    
    @IBAction func clickLogin(_ sender: Any) {
        let vc = LoginViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func clickCamera(sender: UIButton){
        let vc = UIImagePickerController()
        vc.delegate = self
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        present(vc, animated: true, completion: nil)
    }
}
extension UserViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "InforCell", for: indexPath) as? InforCell else {
                return UITableViewCell()
            }
            cell.btnCamera.addTarget(self, action: #selector(clickCamera(sender:)), for: .touchUpInside)
            cell.bindData(name,imageUser)
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ItemMenuCell", for: indexPath) as? ItemMenuCell else {
                return UITableViewCell()
            }
            cell.bindData(menu[indexPath.row-1])
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 300
        }else{
            return 80
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3{
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
                let vc = LoginViewController()
                navigationController?.pushViewController(vc, animated: true)
            } catch let signOutError as NSError {
                print("Error signing out: %@", signOutError)
            }
        }else if indexPath.row == 2{
            let vc = ChangePasswordViewController()
            navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1{
            let vc = InforViewController()
            vc.user = userInfor
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
extension UserViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage {
            let data = image.pngData()
            updateProfileInfo(withImage: data, name: nil)
            inforTable.reloadData()
            dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
