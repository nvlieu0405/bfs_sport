//
//  ChangePasswordViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 29/04/2022.
//

import UIKit
import FirebaseAuth
class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var confirmpass: UITextField!
    @IBOutlet weak var newpass: UITextField!
    @IBOutlet weak var nowpass: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        nowpass.delegate = self
        newpass.delegate = self
        confirmpass.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        indicator.isHidden = true
        title = "Change Password"
        navigationController?.navigationBar.isHidden = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    func changePassword(email: String, currentPassword: String, newPassword: String, completion: @escaping (Error?) -> Void) {
        let credential = EmailAuthProvider.credential(withEmail: email, password: currentPassword)
        Auth.auth().currentUser?.reauthenticate(with: credential, completion: { (result, error) in
            if let error = error {
                self.errorLabel.text = error.localizedDescription
                self.errorLabel.alpha = 1
            }
            else {
                Auth.auth().currentUser?.updatePassword(to: newPassword, completion: { (error) in
                    completion(error)
                })
            }
        })
    }
    @IBAction func clickAccept(_ sender: Any) {
        let check = SignInViewController()
        let user = Auth.auth().currentUser
        guard let user = user else{
            return
        }
        self.indicator.startAnimating()
        self.indicator.isHidden = false
        if self.newpass.text?.trimmingCharacters(in: .whitespacesAndNewlines) == self.confirmpass.text?.trimmingCharacters(in: .whitespacesAndNewlines), check.isPasswordValidate(self.newpass.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""){
                self.indicator.isHidden = true
                self.changePassword(email: user.email ?? "", currentPassword: self.nowpass.text ?? "", newPassword: self.newpass.text ?? "") { (error) in
                        let alert = UIAlertController(title: "Success!", message: "Change password complete!", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default){ _ in
                            self.navigationController?.popViewController(animated: true)
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true)
                }
            
        }else{
            self.indicator.isHidden = true
            let alert = UIAlertController(title: "Error!", message: "Error validate!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default){ _ in
                
            }
            alert.addAction(okAction)
            self.present(alert, animated: true)
        }
    }
}

extension ChangePasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
}
