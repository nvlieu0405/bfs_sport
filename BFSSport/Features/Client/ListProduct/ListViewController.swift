//
//  ListViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 12/04/2022.
//

import UIKit

class ListViewController: UIViewController {
 
    @IBOutlet weak var listProductTable: UITableView!
    var list: [ProductElement] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
    }
    func setupTable(){
        listProductTable.delegate = self
        listProductTable.dataSource = self
        listProductTable.register(UINib(nibName: "KindViewCell", bundle: nil), forCellReuseIdentifier: "KindViewCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
}
extension ListViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "KindViewCell", for: indexPath) as? KindViewCell else{
            return UITableViewCell()
        }
        cell.bindData(list[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        160
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailViewController()
        vc.product = list[indexPath.row]
        vc.list = list
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
