//
//  KindViewCell.swift
//  BFSSport
//
//  Created by Văn Liệu on 13/04/2022.
//

import UIKit
import Kingfisher
class KindViewCell: UITableViewCell {

    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var imageStar: UIImageView!
    @IBOutlet weak var priceProduct: UILabel!
    @IBOutlet weak var nameProduct: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func bindData(_ product: ProductElement){
        nameProduct.text = product.name
        priceProduct.text = "Price: \(product.price) $"
        let urlImg: String = "http://soleinsider.com/public/products/\(product.image)"
        let url = URL(string: urlImg)
        let processor = DownsamplingImageProcessor(size: imageProduct.bounds.size)
                     |> RoundCornerImageProcessor(cornerRadius: 0)
        imageProduct.kf.indicatorType = .activity
        imageProduct.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholder"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        let rate = Int(product.yesPercentage) ?? 20
        var nameImage: String = "star1.5"
        if rate < 25 {
            nameImage = "star1.5"
        } else if rate < 45 {
            nameImage = "star2.5"
        } else if rate < 65 {
            nameImage = "star3.5"
        } else if rate < 85 {
            nameImage = "star4.5"
        } else {
            nameImage = "star5.5"
        }
        imageStar.image = UIImage(named: nameImage)
    }
}
