//
//  CartViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 10/04/2022.
//

import UIKit
import FirebaseAuth
class CartViewController: UIViewController {
    
    @IBOutlet weak var cartTableView: UITableView!
    var list: [Cart] = []
    static var sumOfPrice: Float = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        title = "Cart"
        // Do any additional setup after loading the view.
    }
    func getTotalMoney(){
        CartViewController.sumOfPrice = 0
        for item in list{
            CartViewController.sumOfPrice += item.price * Float(item.number)
        }
    }
    func setupTable(){
        cartTableView.delegate = self
        cartTableView.dataSource = self
        cartTableView.register(UINib(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "CartCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        list = DatabaseManager.shared.getListCart()
        cartTableView.reloadData()
        getTotalMoney()
        
    }
    
    @IBAction func clickOrder(_ sender: Any) {
    let user = Auth.auth().currentUser?.uid
       if user == nil{
            let alert = UIAlertController(title: "Nofication!", message: "You need to login to order!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default){ _ in
                let vc = LoginViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            present(alert, animated: true)
       }else{
           let vc = AcceptOrderViewController()
           navigationController?.pushViewController(vc, animated: true)
       }
    }
}
extension CartViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as? CartCell else{
            return UITableViewCell()
        }
        cell.bindData(list[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        120
    }
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        "Total money: \(CartViewController.sumOfPrice) $"
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let update = UIContextualAction(style: .normal, title: "Update") { (action, view, completionHandler) in
            let alert = UIAlertController(title: "Choose Number", message: "Please enter the quantity you want to buy", preferredStyle: UIAlertController.Style.alert )
            let save = UIAlertAction(title: "OK", style: .default) { (alertAction) in
                let textField = alert.textFields![0] as UITextField
                if textField.text != "" {
                    let text: String = textField.text ?? "0"
                    DatabaseManager.shared.updateCart(with: Int(self.list[indexPath.row].id), number: Int(text) ?? 1)
                    self.list = DatabaseManager.shared.getListCart()
                    self.getTotalMoney()
                    self.cartTableView.reloadData()
                } else {
                    print("TF 1 is Empty...")
                }
            }
            alert.addTextField { (textField) in
                textField.placeholder = "Enter number"
                textField.textColor = .black
            }
            alert.addAction(save)
            let cancel = UIAlertAction(title: "Cancel", style: .default) { (alertAction) in }
            alert.addAction(cancel)
            self.present(alert, animated:true, completion: nil)
        }
        
        let delete = UIContextualAction(style: .normal, title: "Delete") { (action, view, completionHandler) in
            let alert = UIAlertController(title: "Error!", message: "Are you sure delete?", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default){_ in
                DatabaseManager.shared.deleteCart(id: Int(self.list[indexPath.row].id))
                self.list.remove(at: indexPath.row)
                self.getTotalMoney()
                self.cartTableView.reloadData()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true)
            }
        delete.image = UIImage(systemName: "trash")
        delete.backgroundColor = .red
        update.image = UIImage(systemName: "square.and.arrow.up")
        update.backgroundColor = .systemGreen
        let swipe = UISwipeActionsConfiguration(actions: [update,delete])
        return swipe
    }
}
