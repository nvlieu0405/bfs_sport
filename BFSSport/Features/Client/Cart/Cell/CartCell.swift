//
//  CartCell.swift
//  BFSSport
//
//  Created by Văn Liệu on 13/04/2022.
//

import UIKit
import Kingfisher
class CartCell: UITableViewCell {

    @IBOutlet weak var numberBuy: UILabel!
    @IBOutlet weak var priceProduct: UILabel!
    @IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    var id: Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func bindData(_ product: Cart){
        id = Int(product.id)
        nameProduct.text = product.name
        priceProduct.text = "Price: \(product.price) $"
        numberBuy.text = " x \(product.number)"
        let urlImg: String = "http://soleinsider.com/public/products/\(product.image ?? "")"
        let url = URL(string: urlImg)
        let processor = DownsamplingImageProcessor(size: imageProduct.bounds.size)
                     |> RoundCornerImageProcessor(cornerRadius: 0)
        imageProduct.kf.indicatorType = .activity
        imageProduct.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholder"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
    }
    
}
