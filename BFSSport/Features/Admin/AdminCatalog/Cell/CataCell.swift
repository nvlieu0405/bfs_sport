//
//  CataCell.swift
//  BFSSport
//
//  Created by Văn Liệu on 02/05/2022.
//

import UIKit

class CataCell: UITableViewCell {

    @IBOutlet weak var titleCata: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func bindData(_ data: String){
        titleCata.text = data
    }
}
