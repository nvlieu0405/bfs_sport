//
//  AdminCatalogViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 02/05/2022.
//

import UIKit
import Firebase
import FirebaseDatabase
class AdminCatalogViewController: UIViewController {
    var number: Int = 0
    @IBOutlet weak var btnAdd: UIButton!
    var ref: DatabaseReference!
    @IBOutlet weak var catalogTable: UITableView!
    var listCata: [CatalogEntity] = []
    var listProduct: [ProductElement] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        // Do any additional setup after loading the view.
        setupTable()
        btnAdd.layer.cornerRadius = btnAdd.bounds.height / 2
    }
    @IBAction func clickAdd(_ sender: Any) {
        alertWithCata()
    }
    func setupTable(){
        catalogTable.dataSource = self
        catalogTable.delegate = self
        catalogTable.register(UINib(nibName: "CataCell", bundle: nil), forCellReuseIdentifier: "CataCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        title = "Catalog Management"
        getData()
        getDataProduct()
    }
    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    private func getData(){
        listCata.removeAll()
        number = 0
        ref.child("DanhMuc").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            let cata = snapshot.value as? [NSDictionary?]
            
            if cata == nil {
                let cata2 = snapshot.value as? NSDictionary
                guard let cata2 = cata2 else {
                    return
                }
                for item2 in cata2 {
                    let item = item2.value as? NSDictionary
                    if item != nil {
                        let title: String = item?.value(forKey: "title") as? String ?? ""
                        let id: Int = item?.value(forKey: "id") as? Int ?? 0
                        let newCat = CatalogEntity(id: id,title: title)
                        self.number = id + 1
                        self.addToList(newCat)
                    }
                }
            }else{
                guard let cata = cata else {
                    return
                }
                for item in cata {
                    if item != nil {
                        let title: String = item?.value(forKey: "title") as? String ?? ""
                        let id: Int = item?.value(forKey: "id") as? Int ?? 0
                        let newCat = CatalogEntity(id: id,title: title)
                        self.number = id + 1
                        self.addToList(newCat)
                    }
                }
            }
            self.catalogTable.reloadData()
        })
    }
    func addToList(_ newCat: CatalogEntity){
        listCata.append(newCat)
    }
    
    func alertWithCata() {
        let alert = UIAlertController(title: "Add Catalogy", message: "Please catalogy", preferredStyle: UIAlertController.Style.alert )
        let save = UIAlertAction(title: "OK", style: .default) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            if textField.text != "" {
                self.ref.child("DanhMuc").child("\(self.number)").child("title").setValue(textField.text)
                self.ref.child("DanhMuc").child("\(self.number)").child("id").setValue(self.number)
                self.getData()
            } else {
                print("TF 1 is Empty...")
            }
        }
        alert.addTextField { (textField) in
            textField.placeholder = "Enter catalogy's name"
            textField.textColor = .black
        }
        alert.addAction(save)
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (alertAction) in }
        alert.addAction(cancel)
        self.present(alert, animated:true, completion: nil)
    }
    
    func checkNumberInCata(_ cata: CatalogEntity) -> Bool {
        let id = String(cata.id)
        
        for item in listProduct {
            if item.type == id {
                return false
            }
        }
        return true
        
    }
    
    func getDataProduct(){
        listProduct.removeAll()
        ref.child("sanpham").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            let userName = snapshot.value as? NSDictionary
            guard let userName = userName else{
                return
            }
            for item in userName{
                let product = item.value as? NSDictionary
                let idp: String = product?.value(forKey: "productID") as? String ?? ""
                let name: String = product?.value(forKey: "name") as? String ?? ""
                let image: String = product?.value(forKey: "image") as? String  ?? ""
                let content: String = product?.value(forKey: "content") as? String ?? ""
                let price: String = product?.value(forKey: "price") as? String ?? ""
                let createAt: String = product?.value(forKey: "createAt") as? String ?? ""
                let updateAt: String = product?.value(forKey: "updateAt") as? String ?? ""
                let releaseDate: String = product?.value(forKey: "releaseDate") as? String ?? ""
                let rate: String = product?.value(forKey: "rate") as? String ?? ""
                let type: String = product?.value(forKey: "type") as? String ?? ""
                let qualityAvailable: Int = product?.value(forKey: "quantityAvailable") as? Int ?? 0
                let newPro = ProductElement(id: idp, name: name, image: image, content: content, price: price, type: type, createdAt: createAt, updatedAt: updateAt, productID: idp, releaseDate: releaseDate, yesPercentage: rate, qualityAvailable: qualityAvailable)
                self.number = Int(idp) ?? 100000 + 1
                self.addToList(newPro)
            }
        })
    }
    
    func addToList(_ pro: ProductElement){
        listProduct.append(pro)
    }
    
}
extension AdminCatalogViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listCata.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "List Catalogy"
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CataCell", for: indexPath) as? CataCell else{
            return UITableViewCell()
        }
        cell.bindData(listCata[indexPath.row].title)
        return cell
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let update = UIContextualAction(style: .normal, title: "Update") { (action, view, completionHandler) in
            let alert = UIAlertController(title: "Update Catalogy", message: "Please catalogy", preferredStyle: UIAlertController.Style.alert )
            let save = UIAlertAction(title: "OK", style: .default) { (alertAction) in
                let textField = alert.textFields![0] as UITextField
                if textField.text != "" {
                    self.ref.child("DanhMuc").child("\(self.listCata[indexPath.row].id)").child("title").setValue(textField.text)
                    self.getData()
                } else {
                    let alert2 = UIAlertController(title: "Error", message: "", preferredStyle: UIAlertController.Style.alert )
                    let ok = UIAlertAction(title: "OK", style: .default)
                    alert2.addAction(ok)
                    self.present(alert2, animated: true)
                }
            }
            alert.addTextField { (textField) in
                textField.placeholder = "Enter catalogy's name"
                textField.textColor = .black
            }
            alert.addAction(save)
            let cancel = UIAlertAction(title: "Cancel", style: .default) { (alertAction) in }
            alert.addAction(cancel)
            self.present(alert, animated:true, completion: nil)
        }
        let delete = UIContextualAction(style: .normal, title: "Delete") { (action, view, completionHandler) in
            let alert = UIAlertController(title: "Warning!", message: "Are you sure delete??", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default){_ in
                if self.checkNumberInCata(self.listCata[indexPath.row]) {
                    self.ref.child("DanhMuc").child("\(self.listCata[indexPath.row].id)").setValue(nil)
                    self.listCata.remove(at: indexPath.row)
                    self.getData()
                    self.catalogTable.reloadData()
                }else {
                    let alert2 = UIAlertController(title: "Error", message: "There are products in the catalog! Can not delete!", preferredStyle: UIAlertController.Style.alert )
                    let ok = UIAlertAction(title: "OK", style: .default)
                    alert2.addAction(ok)
                    self.present(alert2, animated: true)
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true)
        }
        update.image = UIImage(systemName: "square.and.arrow.up")
        update.backgroundColor = .systemGreen
        delete.image = UIImage(systemName: "trash")
        delete.backgroundColor = .red
        let swipe = UISwipeActionsConfiguration(actions: [delete,update])
        return swipe
    }
}
