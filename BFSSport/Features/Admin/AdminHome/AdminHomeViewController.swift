//
//  AdminHomeViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 02/05/2022.
//

import UIKit

class AdminHomeViewController: UIViewController {

    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var menuTable: UITableView!
    var listMenu: [MenuTable] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        btnLogout.layer.cornerRadius = btnLogout.bounds.height/2
        listMenu = [MenuTable(title: "Product Management", image: ""),
                    MenuTable(title: "Catalog Management", image: ""),
                    MenuTable(title: "Order Management", image: "")]
        // Do any additional setup after loading the view.
        setupTable()
    }
    @IBAction func clickLogout(_ sender: Any) {
        let vc = LoginViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    func setupTable(){
        menuTable.dataSource = self
        menuTable.delegate = self
        menuTable.register(UINib(nibName: "AdminHomeCell", bundle: nil), forCellReuseIdentifier: "AdminHomeCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.isHidden = true
    }
}
extension AdminHomeViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listMenu.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AdminHomeCell", for: indexPath) as? AdminHomeCell else{
            return UITableViewCell()
        }
        cell.bindData(listMenu[indexPath.row].title)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row{
        case 0:
            let vc = AdminProductViewController()
            navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = AdminCatalogViewController()
            navigationController?.pushViewController(vc, animated: true)
        case 2:
            let vc = AdminOrderViewController()
            navigationController?.pushViewController(vc, animated: true)
        default:
            let vc = AdminProductViewController()
        }
       
    }
}
