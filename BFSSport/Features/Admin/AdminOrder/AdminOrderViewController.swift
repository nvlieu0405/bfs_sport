//
//  AdminOrderViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 02/05/2022.
//


import UIKit
import FirebaseAuth
import FirebaseDatabase
class AdminOrderViewController: UIViewController {
    @IBOutlet weak var orderTable: UITableView!
    var ref: DatabaseReference!
    var listOrder: [OrderEntity] = []
    var number: Int = 0
    var listProduct: [ProductElement] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        // Do any additional setup after loading the view.
        setupTable()
    }
    func setupTable(){
        orderTable.delegate = self
        orderTable.dataSource = self
        orderTable.register(UINib(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        title = "Order Management"
        getListOrder()
        getDataProduct()
    }
    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    func getListOrder(){
        listOrder.removeAll()
        ref.child("DonHang").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return;
            }
            let order = snapshot.value as? NSDictionary
            guard let order = order else {
                return
            }
            
            for item2 in order{
                let item1 = item2.value as? NSDictionary
                
                if item1 == nil {
                    let item11 = item2.value as? [NSDictionary?]
                    guard let item11 = item11 else {
                        return
                    }
                    for item in item11{
                        // let item = item3.value as? NSDictionary
                        if item != nil {
                            let accountID: String = item?.value(forKey: "id") as? String ?? ""
                            let orderID: Int = item?.value(forKey: "orderId") as? Int ?? 0
                            let addressCustomer = item?.value(forKey: "addressCustomer") as? String ?? ""
                            let nameCustomer = item?.value(forKey: "nameCustomer") as? String ?? ""
                            let numberphone = item?.value(forKey: "numberphone") as? String ?? ""
                            let createAt = item?.value(forKey: "createAt") as? String ?? ""
                            let status = item?.value(forKey: "status") as? Int ?? 0
                            let totalMoney = item?.value(forKey: "totalMoney") as? Float ?? 0.0
                            var listP: [CartEntity] = []
                            let list = item?.value(forKey: "list") as? NSDictionary
                            guard let list = list else{
                                return
                            }
                            for product2 in list{
                                let product = product2.value as? NSDictionary
                                let id: Int = product?.value(forKey:"id") as? Int ?? 0
                                let image: String = product?.value(forKey: "image") as? String ?? ""
                                let name: String = product?.value(forKey: "name") as? String ?? ""
                                let number: Int = product?.value(forKey: "number") as? Int ?? 0
                                let price: Int = product?.value(forKey: "price") as? Int ?? 0
                                let cart = CartEntity(id: id, number: number, price: price, image: image, name: name)
                                listP.append(cart)
                            }
                            let orderEntity = OrderEntity(orderId: orderID, accountId: accountID, addressCustomer: addressCustomer, nameCustomer: nameCustomer, numberphone: numberphone, orderStatus: status,totalMoney: Float(totalMoney) ,createAt: createAt, list: listP)
                            self.addToList(orderEntity)
                        }
                    }
                }else {
                    guard let item1 = item1 else {
                        return
                    }
                    for item3 in item1{
                        let item = item3.value as? NSDictionary
                        let accountID: String = item?.value(forKey: "id") as? String ?? ""
                        let orderID: Int = item?.value(forKey: "orderId") as? Int ?? 0
                        let addressCustomer = item?.value(forKey: "addressCustomer") as? String ?? ""
                        let nameCustomer = item?.value(forKey: "nameCustomer") as? String ?? ""
                        let numberphone = item?.value(forKey: "numberphone") as? String ?? ""
                        let createAt = item?.value(forKey: "createAt") as? String ?? ""
                        let status = item?.value(forKey: "status") as? Int ?? 0
                        let totalMoney = item?.value(forKey: "totalMoney") as? Float ?? 0.0
                        var listP: [CartEntity] = []
                        let list = item?.value(forKey: "list") as? NSDictionary
                        guard let list = list else{
                            return
                        }
                        for product2 in list{
                            let product = product2.value as? NSDictionary
                            let id: Int = product?.value(forKey:"id") as? Int ?? 0
                            let image: String = product?.value(forKey: "image") as? String ?? ""
                            let name: String = product?.value(forKey: "name") as? String ?? ""
                            let number: Int = product?.value(forKey: "number") as? Int ?? 0
                            let price: Int = product?.value(forKey: "price") as? Int ?? 0
                            let cart = CartEntity(id: id, number: number, price: price, image: image, name: name)
                            listP.append(cart)
                        }
                        let orderEntity = OrderEntity(orderId: orderID, accountId: accountID, addressCustomer: addressCustomer, nameCustomer: nameCustomer, numberphone: numberphone, orderStatus: status,totalMoney: Float(totalMoney) ,createAt: createAt, list: listP)
                        self.addToList(orderEntity)
                    }
                }
            }
            self.orderTable.reloadData()
        })
    }
    func addToList(_ order: OrderEntity){
        listOrder.append(order)
    }
}
extension AdminOrderViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listOrder.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as? OrderCell else{
            return UITableViewCell()
        }
        cell.bindData(listOrder[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "List Order"
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let update = UIContextualAction(style: .normal, title: "Update") { (action, view, completionHandler) in
            let alert = UIAlertController(title: "Update Status", message: "Please choose status", preferredStyle: UIAlertController.Style.alert )
            let wait = UIAlertAction(title: "Waiting", style: .default) { (alertAction) in
                let status: Int = 0
                self.ref.child("DonHang").child("\(self.listOrder[indexPath.row].accountId)").child("\(self.listOrder[indexPath.row].orderId)").child("status").setValue(status)
                
                let alert2 = UIAlertController(title: "Success", message: "", preferredStyle: UIAlertController.Style.alert )
                let ok = UIAlertAction(title: "OK", style: .default){ _ in
                    
                }
                alert2.addAction(ok)
                self.present(alert2, animated: true, completion: nil)
                self.getListOrder()
                
            }
            alert.addAction(wait)
            
            let delivering = UIAlertAction(title: "Accept", style: .default) { (alertAction) in
                let status: Int = 1
                self.ref.child("DonHang").child("\(self.listOrder[indexPath.row].accountId)").child("\(self.listOrder[indexPath.row].orderId)").child("status").setValue(status)
                if self.listOrder[indexPath.row].orderStatus == 0 {
                    for item in self.listOrder[indexPath.row].list{
                        let a = self.getNumberById(item.id)
                        let update = a - item.number
                        let childValue = ["sanpham/\(item.id)/quantityAvailable": update]
                        self.ref.updateChildValues(childValue)
                    }
                }
                let alert2 = UIAlertController(title: "Success", message: "", preferredStyle: UIAlertController.Style.alert )
                let ok = UIAlertAction(title: "OK", style: .default){ _ in
                    
                }
                alert2.addAction(ok)
                self.present(alert2, animated: true, completion: nil)
                self.getListOrder()
            }
            alert.addAction(delivering)
            
            let delivered = UIAlertAction(title: "Delivered", style: .default) { (alertAction) in
                let status: Int = 2
                self.ref.child("DonHang").child("\(self.listOrder[indexPath.row].accountId)").child("\(self.listOrder[indexPath.row].orderId)").child("status").setValue(status)
                let alert2 = UIAlertController(title: "Success", message: "", preferredStyle: UIAlertController.Style.alert )
                let ok = UIAlertAction(title: "OK", style: .default){ _ in
                    
                }
                alert2.addAction(ok)
                self.present(alert2, animated: true, completion: nil)
                self.getListOrder()
                
            }
            alert.addAction(delivered)
            let cancelled = UIAlertAction(title: "Cancelled", style: .default) { (alertAction) in
                let status: Int = 3
                self.ref.child("DonHang").child("\(self.listOrder[indexPath.row].accountId)").child("\(self.listOrder[indexPath.row].orderId)").child("status").setValue(status)
                if self.listOrder[indexPath.row].orderStatus != 0, self.listOrder[indexPath.row].orderStatus != 3{
                    for item in self.listOrder[indexPath.row].list{
                        let a = self.getNumberById(item.id)
                        let update = a + item.number
                        let childValue = ["sanpham/\(item.id)/quantityAvailable": update]
                        self.ref.updateChildValues(childValue)
                        
                    }
                }
                let alert2 = UIAlertController(title: "Success", message: "", preferredStyle: UIAlertController.Style.alert )
                let ok = UIAlertAction(title: "OK", style: .default){ _ in
                    
                }
                alert2.addAction(ok)
                self.present(alert2, animated: true, completion: nil)
                self.getListOrder()
            }
            alert.addAction(cancelled)
            self.present(alert, animated:true, completion: nil)
        }
        let delete = UIContextualAction(style: .normal, title: "Delete") { (action, view, completionHandler) in
            let alert = UIAlertController(title: "Warning!", message: "Are you sure delete??", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default){_ in
                self.ref.child("DonHang").child("\(self.listOrder[indexPath.row].accountId)").child("\(self.listOrder[indexPath.row].orderId)").setValue(nil)
                self.listOrder.remove(at: indexPath.row)
                //                self.getListOrder()
                self.orderTable.reloadData()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true)
        }
        update.image = UIImage(systemName: "square.and.arrow.up")
        update.backgroundColor = .systemGreen
        delete.image = UIImage(systemName: "trash")
        delete.backgroundColor = .red
        let swipe = UISwipeActionsConfiguration(actions: [delete,update])
        return swipe
    }
    func getNumberById(_ id: Int) -> Int{
        for item in listProduct {
            if Int(item.id) == id {
                return item.qualityAvailable
            }
        }
        return 100
    }
    func getDataProduct(){
        listProduct.removeAll()
        ref.child("sanpham").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            let userName = snapshot.value as? NSDictionary
            guard let userName = userName else{
                return
            }
            for item in userName{
                let product = item.value as? NSDictionary
                let idp: String = product?.value(forKey: "productID") as? String ?? ""
                let name: String = product?.value(forKey: "name") as? String ?? ""
                let image: String = product?.value(forKey: "image") as? String  ?? ""
                let content: String = product?.value(forKey: "content") as? String ?? ""
                let price: String = product?.value(forKey: "price") as? String ?? ""
                let createAt: String = product?.value(forKey: "createAt") as? String ?? ""
                let updateAt: String = product?.value(forKey: "updateAt") as? String ?? ""
                let releaseDate: String = product?.value(forKey: "releaseDate") as? String ?? ""
                let rate: String = product?.value(forKey: "rate") as? String ?? ""
                let type: String = product?.value(forKey: "type") as? String ?? ""
                let qualityAvailable: Int = product?.value(forKey: "quantityAvailable") as? Int ?? 0
                let newPro = ProductElement(id: idp, name: name, image: image, content: content, price: price, type: type, createdAt: createAt, updatedAt: updateAt, productID: idp, releaseDate: releaseDate, yesPercentage: rate, qualityAvailable: qualityAvailable)
                self.addToList(newPro)
            }
        })
    }
    
    func addToList(_ pro: ProductElement){
        listProduct.append(pro)
    }
}
