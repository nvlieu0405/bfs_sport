//
//  AdminProductViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 02/05/2022.
//

import UIKit
import Alamofire
import FirebaseDatabase
class AdminProductViewController: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnAdd: UIButton!
    var ref: DatabaseReference!
    @IBOutlet weak var productTable: UITableView!
    var listProduct: [ProductElement] = []
    var listFilter: [ProductElement] = []
    var isSearchMode: Bool = false
    var number: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        btnAdd.layer.cornerRadius = btnAdd.bounds.height / 2
        ref = Database.database().reference()
       setupTable()
        
    }
    func setupTable(){
        searchBar.delegate = self
        productTable.dataSource = self
        productTable.delegate = self
        productTable.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        title = "Product Management"
        getData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    func getData(){
        listProduct.removeAll()
        ref.child("sanpham").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            let userName = snapshot.value as? NSDictionary
            guard let userName = userName else{
                return
            }
            for item in userName{
                let product = item.value as? NSDictionary
                let idp: String = product?.value(forKey: "productID") as? String ?? ""
                let name: String = product?.value(forKey: "name") as? String ?? ""
                let image: String = product?.value(forKey: "image") as? String  ?? ""
                let content: String = product?.value(forKey: "content") as? String ?? ""
                let price: String = product?.value(forKey: "price") as? String ?? ""
                let createAt: String = product?.value(forKey: "createAt") as? String ?? ""
                let updateAt: String = product?.value(forKey: "updateAt") as? String ?? ""
                let releaseDate: String = product?.value(forKey: "releaseDate") as? String ?? ""
                let rate: String = product?.value(forKey: "rate") as? String ?? ""
                let type: String = product?.value(forKey: "type") as? String ?? ""
                let qualityAvailable: Int = product?.value(forKey: "quantityAvailable") as? Int ?? 0
                let newPro = ProductElement(id: idp, name: name, image: image, content: content, price: price, type: type, createdAt: createAt, updatedAt: updateAt, productID: idp, releaseDate: releaseDate, yesPercentage: rate, qualityAvailable: qualityAvailable)
                self.number = Int(idp) ?? 100000 + 1
                self.addToList(newPro)
            }
            self.productTable.reloadData()
        })
    }
    
    func addToList(_ pro: ProductElement){
        listProduct.append(pro)
    }
    @IBAction func invokeAddButton(_ sender: Any) {
        let vc = AdminDetailViewController()
        vc.isAdd = true
        navigationController?.pushViewController(vc, animated: true)
    }
}
extension AdminProductViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchMode{
            return listFilter.count
        }
        return listProduct.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as? ProductCell else{
            return UITableViewCell()
        }
        if isSearchMode{
            cell.bindData(listFilter[indexPath.row])
        }else{
            cell.bindData(listProduct[indexPath.row])
        }
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "List Product"
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = AdminDetailViewController()
        if isSearchMode{
            vc.product = listFilter[indexPath.row]
        }else{
            vc.product = listProduct[indexPath.row]
        }
        vc.isAdd = false
        vc.id = number
       
        navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .normal, title: "Delete") { (action, view, completionHandler) in
            let alert = UIAlertController(title: "Warning!", message: "Are you sure delete??", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default){_ in
                if self.isSearchMode {
                    self.ref.child("sanpham").child("\(self.listFilter[indexPath.row].id)").setValue(nil)
                    self.listFilter.remove(at: indexPath.row)
                }else {
                    self.ref.child("sanpham").child("\(self.listProduct[indexPath.row].id)").setValue(nil)
                    self.listProduct.remove(at: indexPath.row)
                }
                self.getData()
                self.productTable.reloadData()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true)
        }
        delete.image = UIImage(systemName: "trash")
        delete.backgroundColor = .red
        let swipe = UISwipeActionsConfiguration(actions: [delete])
        return swipe
    }
}


extension AdminProductViewController:UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == ""{
            isSearchMode = false
            self.searchBar.showsCancelButton = false
            self.productTable.reloadData()
        }else{
            isSearchMode = true
            listFilter = listProduct.filter({(contact: ProductElement)->Bool in
                contact.name.lowercased().contains(searchText.lowercased())
            })
            self.productTable.reloadData()
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
}
