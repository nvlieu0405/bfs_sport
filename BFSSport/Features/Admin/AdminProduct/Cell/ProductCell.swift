//
//  ProductCell.swift
//  BFSSport
//
//  Created by Văn Liệu on 02/05/2022.
//

import UIKit
import Kingfisher
class ProductCell: UITableViewCell {
    @IBOutlet weak var quantityAvailable: UILabel!
    
    @IBOutlet weak var nameProoduct: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func bindData(_ data: ProductElement){
        nameProoduct.text = "Name: \(data.name)"
        quantityAvailable.text = "Quantity Available: \(data.qualityAvailable)"
        let urlImg: String = "http://soleinsider.com/public/products/\(data.image)"
        let url = URL(string: urlImg)
        let processor = DownsamplingImageProcessor(size: imageProduct.bounds.size)
                     |> RoundCornerImageProcessor(cornerRadius: 10)
        imageProduct.kf.indicatorType = .activity
        imageProduct.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholder"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
    }
    
}
