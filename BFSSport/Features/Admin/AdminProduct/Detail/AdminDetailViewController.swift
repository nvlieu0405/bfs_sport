//
//  AdminDetailViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 03/05/2022.
//

import UIKit
import Kingfisher
import FirebaseDatabase
class AdminDetailViewController: UIViewController {
    var id : Int = 100000
    var productID: String = ""
    var ref: DatabaseReference!
    var isAdd: Bool = false
    var listCata: [CatalogEntity] = []
    
    @IBOutlet weak var typeCollectionView: UICollectionView!
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var btnReebok: UIButton!
    @IBOutlet weak var btnNike: UIButton!
    @IBOutlet weak var btnJordan: UIButton!
    @IBOutlet weak var btnAdidas: UIButton!
    @IBOutlet weak var textRate: UITextField!
    @IBOutlet weak var textNumber: UITextField!
    @IBOutlet weak var textPrice: UITextField!
    @IBOutlet weak var textContent: UITextField!
    @IBOutlet weak var textName: UITextField!
    var type: String = ""
    var image: String = "Air-Jordan-1-Mid-WMNS-Homecoming-DC1426-100-Release-Date-1-1068x713.jpg"
    var product: ProductElement?
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        typeCollectionView.register(UINib(nibName: "TypeCell", bundle: nil), forCellWithReuseIdentifier: "TypeCell")
        typeCollectionView.dataSource = self
        typeCollectionView.delegate = self
        
        textName.delegate = self
        textRate.delegate = self
        textPrice.delegate = self
        textNumber.delegate = self
        textContent.delegate = self
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    @IBAction func invokeAdd(_ sender: Any) {
        if textName.text == "" || textRate.text == "" || textContent.text == "" || textPrice.text == "" || textNumber.text == ""{
            let alert = UIAlertController(title: "Error!", message: "Error validate!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .destructive){_ in
                
            }
            alert.addAction(okAction)
            present(alert, animated: true)
        }else{
            if isAdd{
                addProductFirebase()
            }else{
                updateProduct()
            }
        }
        
        
    }
    func updateProduct(){

        let name = textName.text ?? ""
        let content = textContent.text ?? ""
        let price: Double = Double(textPrice.text ?? "0.0") ?? 0.0
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let rate = Int(textRate.text ?? "0") ?? 0
        let quantityAvailable: Int = Int(textNumber.text ?? "0") ?? 0
        
        if name != "", content != "", price != 0.0, rate != 0, quantityAvailable != 0 {
            ref.child("sanpham/\(productID)/type").setValue(type)
            ref.child("sanpham/\(productID)/name").setValue(name)
            ref.child("sanpham/\(productID)/content").setValue("\(name) \n \(content) \n Price: $\(price)")
            ref.child("sanpham").child("\(productID)").child("price").setValue("\(price)")
            ref.child("sanpham").child("\(productID)").child("updateAt").setValue("\(dateFormatter.string(from:currentDate))")
            ref.child("sanpham").child("\(productID)").child("rate").setValue("\(rate)")
            ref.child("sanpham").child("\(productID)").child("quantityAvailable").setValue(quantityAvailable)
            let alert = UIAlertController(title: "Success!", message: "Update complete!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default){_ in
                
            }
            alert.addAction(okAction)
            present(alert, animated: true)
        } else {
            let alert2 = UIAlertController(title: "Error", message: "", preferredStyle: UIAlertController.Style.alert )
            let ok = UIAlertAction(title: "OK", style: .default)
            alert2.addAction(ok)
            self.present(alert2, animated: true)
        }
    }
    private func addProductFirebase(){
        
        let name = textName.text ?? ""
        let content = textContent.text ?? ""
        let price: Double = Double(textPrice.text ?? "0.0") ?? 0.0
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let rate = Int(textRate.text ?? "0") ?? 0
        let quantityAvailable: Int = Int(textNumber.text ?? "0") ?? 0
        
        if name != "", content != "", price != 0.0, rate != 0, quantityAvailable != 0 {
            self.ref.child("sanpham").child("\(id)").child("name").setValue(name)
            self.ref.child("sanpham").child("\(id)").child("productID").setValue("\(id)")
            self.ref.child("sanpham").child("\(id)").child("image").setValue(image)
            self.ref.child("sanpham").child("\(id)").child("content").setValue("\(name) \n \(content) \n Price: $\(price)")
            self.ref.child("sanpham").child("\(id)").child("price").setValue("\(price)")
            self.ref.child("sanpham").child("\(id)").child("createAt").setValue(dateFormatter.string(from:currentDate))
            self.ref.child("sanpham").child("\(id)").child("updateAt").setValue("\(dateFormatter.string(from:currentDate))")
            
            self.ref.child("sanpham").child("\(id)").child("releaseDate").setValue(dateFormatter.string(from:currentDate))
            self.ref.child("sanpham").child("\(id)").child("rate").setValue("\(rate)")
            self.ref.child("sanpham").child("\(id)").child("quantityAvailable").setValue(quantityAvailable)
            self.ref.child("sanpham").child("\(id)").child("type").setValue(type)
            id += 1
            let alert = UIAlertController(title: "Success!", message: "Add complete!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default){_ in
                
            }
            alert.addAction(okAction)
            present(alert, animated: true)
        }
    }
    
    @IBAction func invokeBackButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        title = "Detail"
        navigationController?.navigationBar.isHidden = true
        getCataData()
        getData()
        if isAdd{
            btnUpdate.titleLabel?.text = "Add"
        }else{
            btnUpdate.titleLabel?.text = "Update"
        }
        type = product?.type ?? "1"
    }
    
    func getData(){
        let urlImg1: String = "http://soleinsider.com/public/products/\(image)"
        let url1 = URL(string: urlImg1)
        let processor1 = DownsamplingImageProcessor(size: imageProduct.bounds.size)
        |> RoundCornerImageProcessor(cornerRadius: 0)
        imageProduct.kf.indicatorType = .activity
        imageProduct.kf.setImage(
            with: url1,
            placeholder: UIImage(named: "default"),
            options: [
                .processor(processor1),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        guard let product = product else {
            return
        }
        productID = product.id
        textName.text = product.name
        textContent.text = product.content.html2String.htmlToString
        textPrice.text = product.price
        textNumber.text = "\(product.qualityAvailable)"
        textRate.text = product.yesPercentage
        let urlImg: String = "http://soleinsider.com/public/products/\(product.image)"
        let url = URL(string: urlImg)
        let processor = DownsamplingImageProcessor(size: imageProduct.bounds.size)
        |> RoundCornerImageProcessor(cornerRadius: 0)
        imageProduct.kf.indicatorType = .activity
        imageProduct.kf.setImage(
            with: url,
            placeholder: UIImage(named: "default"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        typeLabel.text = getNameCata(product.type)
    }
    
    func getNameCata(_ id: String) -> String {
        for item in listCata {
            if item.id == Int(id) {
                return item.title
            }
        }
        return ""
    }
}

extension AdminDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        listCata.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TypeCell", for: indexPath) as? TypeCell else {
            return UICollectionViewCell()
        }
        cell.bindData(listCata[indexPath.row])
        cell.delegate = self
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: typeCollectionView.bounds.width / 3, height: typeCollectionView.bounds.height)
    }
}
extension AdminDetailViewController {
    private func getCataData(){
        listCata.removeAll()
        ref.child("DanhMuc").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            let cata = snapshot.value as? [NSDictionary?]
            
            if cata == nil {
                let cata2 = snapshot.value as? NSDictionary
                guard let cata2 = cata2 else {
                    return
                }
                for item2 in cata2 {
                    let item = item2.value as? NSDictionary
                    if item != nil {
                        let title: String = item?.value(forKey: "title") as? String ?? ""
                        let id: Int = item?.value(forKey: "id") as? Int ?? 0
                        let newCat = CatalogEntity(id: id,title: title)
                        self.addToList(newCat)
                    }
                }
            }else{
                guard let cata = cata else {
                    return
                }
                for item in cata {
                    if item != nil {
                        let title: String = item?.value(forKey: "title") as? String ?? ""
                        let id: Int = item?.value(forKey: "id") as? Int ?? 0
                        let newCat = CatalogEntity(id: id,title: title)
                        self.addToList(newCat)
                    }
                }
            }
            self.getData()
            self.typeCollectionView.reloadData()
            
        })
    }
    func addToList(_ newCat: CatalogEntity){
        listCata.append(newCat)
    }
}
extension AdminDetailViewController: TypeCellProtocol {
    func changeType(_ cata: CatalogEntity) {
        type = String(cata.id)
        typeLabel.text = cata.title
    }
}
extension AdminDetailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
}
