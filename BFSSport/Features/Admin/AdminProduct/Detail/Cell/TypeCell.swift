//
//  TypeCell.swift
//  BFSSport
//
//  Created by Văn Liệu on 10/05/2022.
//

import UIKit

protocol TypeCellProtocol {
    func changeType(_ cata: CatalogEntity)
}

class TypeCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    var delegate: TypeCellProtocol?
    var cata: CatalogEntity?
    @IBOutlet weak var btnType: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func bindData(_ cata1: CatalogEntity) {
        cata = cata1
        nameLabel.text = cata1.title
    }
    @IBAction func invokeButtonType(_ sender: UIButton) {
        guard let cata = cata else {
            return
        }
        delegate?.changeType(cata)
    }
    
}
