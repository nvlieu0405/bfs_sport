//
//  LoginViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 13/04/2022.
//

import UIKit
import FirebaseAuth
class LoginViewController: UIViewController {
    
    @IBOutlet weak var btnEye: UIButton!
    var isEye: Bool = true
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var textEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Log In"
        textPassword.delegate = self
        textEmail.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.isHidden = false
        errorLabel.alpha = 0
    }
    override func viewDidDisappear(_ animated: Bool) {
        
//        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func btnEye(_ sender: Any) {
        if isEye {
            textPassword.isSecureTextEntry = false
            isEye = !isEye
            btnEye.setImage(UIImage(named: "eye"), for: .normal)
        }else {
            textPassword.isSecureTextEntry = true
            btnEye.setImage(UIImage(named: "eye.slash"), for: .normal)
            isEye = !isEye
        }
    }
    
    @IBAction func clickLogin(_ sender: Any) {
        let email = textEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = textPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if email.lowercased() == "admin" , password == "Lieu1234@"{
            let vc = AdminHomeViewController()
            self.tabBarController?.tabBar.isHidden = true
            navigationController?.pushViewController(vc, animated: true)
        }else{
            Auth.auth().signIn(withEmail: email, password: password){ (result, error) in
                if error != nil {
                    self.errorLabel.text = error?.localizedDescription
                    self.errorLabel.alpha = 1
                }else{
                    let vc = HomeViewController()
                    CheckLogin.checkLogin =  true
                    self.tabBarController?.tabBar.isHidden = false
                    self.tabBarController?.selectedIndex = 0
                    self.navigationController?.pushViewController(vc, animated: true)
                    self.navigationController?.isToolbarHidden = true
                }
                
            }
        }
        
    }
    @IBAction func clickSignUp(_ sender: Any) {
        let vc = SignInViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickForgot(_ sender: Any) {
        alertWithTF()
    }
    func alertWithTF() {
        //Step : 1
        let alert = UIAlertController(title: "Reset Password", message: "Please your email", preferredStyle: UIAlertController.Style.alert )
        //Step : 2
        let save = UIAlertAction(title: "OK", style: .default) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            if textField.text != "" {
                Auth.auth().sendPasswordReset(withEmail: textField.text ?? "") { error in
                    let alert2 = UIAlertController(title: "Success!", message: "Your request has been sent to your email!", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default)
                    alert2.addAction(ok)
                    self.present(alert2,animated: true,completion: nil)
                }
            } else {
                print("TF 1 is Empty...")
            }
        }

        //Step : 3
        //For first TF
        alert.addTextField { (textField) in
            textField.placeholder = "Enter your email"
            textField.textColor = .red
        }
        //Step : 4
        alert.addAction(save)
        //Cancel action
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (alertAction) in }
        alert.addAction(cancel)

        self.present(alert, animated:true, completion: nil)

    }
}
class CheckLogin{
    static var checkLogin: Bool = false
}
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
}
