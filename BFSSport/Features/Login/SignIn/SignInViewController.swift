//
//  SignInViewController.swift
//  BFSSport
//
//  Created by Văn Liệu on 13/04/2022.
//

import UIKit
import FirebaseAuth
import Firebase
class SignInViewController: UIViewController {

    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textLastName: UITextField!
    @IBOutlet weak var textFirstName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textEmail.delegate = self
        textPassword.delegate = self
        textLastName.delegate = self
        textFirstName.delegate = self
    }
    func validateFields() -> String?{
        if textEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || textPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || textFirstName.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || textLastName.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please enter full information!"
        }
        let cleanPassword = textPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if !isPasswordValidate(cleanPassword){
            return "Please enter a password of at least 8 characters and in the correct format!"
        }
        return nil
        
    }
    override func viewWillAppear(_ animated: Bool) {
        errorLabel.alpha = 0
        
        navigationController?.navigationBar.isHidden = false
        
        tabBarController?.tabBar.isHidden = true
    }
    override func viewDidDisappear(_ animated: Bool) {
//        tabBarController?.tabBar.isHidden = false
    }
    func isPasswordValidate(_ password: String) -> Bool{
            // least one uppercase,
            // least one digit
            // least one lowercase
            // least one symbol
            //  min 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&<>*~:`-]).{8,}$")
        return passwordTest.evaluate(with: password)
    }
    @IBAction func clickSignUp(_ sender: Any) {
        let error = validateFields()
        if error != nil{
            showError(error!)
        }else{
            let email = textEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = textPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let firstName = textFirstName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastName = textLastName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            Auth.auth().createUser(withEmail: email, password: password){(result,err) in
                if let error = error {
                    self.showError(error)
                }else{
                    guard let result =  result else{
                        return
                    }
                    let db = Firestore.firestore()
                    db.collection("user").document("\(result.user.uid)").setData( ["firstname":firstName,"lastname":lastName,"uid":result.user.uid,"address" : "", "numberphone" : ""]){ (error) in
                        if error !=  nil {
                            self.showError("Error validate")
                        } else {
                            self.showAlert()
                        }
                        
                    }
                    
                }
            }
        }
    }
    func showAlert(){
        let alert = UIAlertController(title: "Success!", message: "Complete!", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default){_ in
            let vc = HomeViewController()
            CheckLogin.checkLogin = true
            self.navigationController?.navigationBar.isHidden = true
            self.tabBarController?.tabBar.isHidden = false
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.pushViewController(vc, animated: true)
        }
        alert.addAction(okAction)
        present(alert, animated: true)
    }
    func showError(_ message: String){
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
}
extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
}
