//
//  DatabaseManager.swift
//  BFSSport
//
//  Created by Văn Liệu on 13/04/2022.
//

import Foundation
import CoreData
final class DatabaseManager{
    static let shared = DatabaseManager()
    private let modelName = "CartModel"
    private let cartEntity = "Cart"
    private lazy var managedObjectModel: NSManagedObjectModel? = {
        guard let modelUrl = Bundle.main.url(forResource: modelName, withExtension: "momd") else{
            return nil
        }
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelUrl)else {
            return nil
        }
        return managedObjectModel
    }()
    
    private lazy var coordinator: NSPersistentStoreCoordinator = {
        let persistentCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel!)
        var documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let persistentStoreUrl = documentDirectoryUrl.appendingPathComponent("\(modelName).sqlite")
        do{
            let options = [NSMigratePersistentStoresAutomaticallyOption:true, NSInferMappingModelAutomaticallyOption: true]
            try persistentCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: persistentStoreUrl, options: options)
        }catch let error{
            debugPrint("Error: create persistenrCoordinator")
        }
        return persistentCoordinator
    }()
    
    private lazy var managedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    private func saveContext() -> Bool{
        if !managedObjectContext.hasChanges{
            return true
        }
        do{
            try managedObjectContext.save()
            return true
        }catch let error{
            debugPrint("Error: Save contact: \(error)")
            managedObjectContext.rollback()
            return false
        }
    }
}
extension DatabaseManager{
    func addCart(id: Int, number: Int,name: String, price: Float, image: String) -> Bool{
        let entity = NSEntityDescription.insertNewObject(forEntityName: cartEntity, into: managedObjectContext) as! Cart
        entity.id = (Int64)(id)
        entity.number = (Int64)(number)
        entity.name = name
        entity.price = price
        entity.image = image
        return saveContext()
    }
    func checkCart(id: Int) -> Bool{
        let fetchRequest = Cart.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %d", id)
        do{
            if let contact = try managedObjectContext.fetch(fetchRequest).first{
                return true
            }
        }catch let error{
            debugPrint("Error: \(error)")
        }
        return false
    }
    func deleteCart(id: Int){
        let fetchRequest = Cart.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %d", id)
        do{
            let favoriteList = try managedObjectContext.fetch(fetchRequest)
            for fa in favoriteList{
                managedObjectContext.delete(fa)
            }
        }catch let error{
            debugPrint("Error: \(error)")
        }
        _ = saveContext()
    }
    func deleteCart(){
        let fetchRequest = Cart.fetchRequest()
        do{
            let favoriteList = try managedObjectContext.fetch(fetchRequest)
            for fa in favoriteList{
                managedObjectContext.delete(fa)
            }
        }catch let error{
            debugPrint("Error: \(error)")
        }
        _ = saveContext()
    }
    func updateCart(with id: Int, number: Int){
        let fetchRequest = Cart.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %d", id)
        do{
            if let cart = try managedObjectContext.fetch(fetchRequest).first{
                cart.number = (Int64)(number)
            }
        }catch let error{
            debugPrint("Error: \(error)")
        }
        _ = saveContext()
    }
    func getListCart() -> [Cart]{
        let fetchRequest = Cart.fetchRequest()
        do{
            let listContact = try  managedObjectContext.fetch(fetchRequest)
            return listContact
        }catch let error{
            debugPrint("Error: get list Contact: \(error)")
        }
        return []
    }
}
