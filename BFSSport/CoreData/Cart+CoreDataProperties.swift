//
//  Cart+CoreDataProperties.swift
//  BFSSport
//
//  Created by Văn Liệu on 13/04/2022.
//
//

import Foundation
import CoreData


extension Cart {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cart> {
        return NSFetchRequest<Cart>(entityName: "Cart")
    }

    @NSManaged public var id: Int64
    @NSManaged public var number: Int64
    @NSManaged public var name: String?
    @NSManaged public var price: Float
    @NSManaged public var image: String?

}

extension Cart : Identifiable {

}
